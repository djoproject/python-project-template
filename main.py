#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from logging import getLogger


logger = getLogger(__name__)


def main():
    logger.info("Hello World !")

    # Add business code here ...

    return 0

if __name__ == "__main__":
    from argparse import ArgumentParser

    description = "Asyncio template main"
    parser = ArgumentParser(description=description)
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug mode')

    # Add more argument here ...

    args = parser.parse_args()

    from logging import DEBUG
    from logging import INFO
    from logging import basicConfig

    # Configure logging
    if args.debug:
        logging_format = ("[%(asctime)s] %(levelname)s [%(name)s:%(lineno)d] "
                          "%(message)s")
        logging_level = DEBUG
    else:
        logging_format = "[%(asctime)s] %(levelname)s %(message)s"
        logging_level = INFO

    basicConfig(level=logging_level, format=logging_format)

    # Let's roll !
    try:
        exit(main())
    except Exception:
        logger.exception("unexpected error")

    exit(1)
