#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from asyncio import CancelledError
from asyncio import current_task
from asyncio import get_running_loop
from logging import getLogger
from signal import SIGINT
from signal import SIGTERM


_LOGGER = getLogger(__name__)


async def main():
    loop = get_running_loop()
    main_task = current_task()
    loop.add_signal_handler(SIGINT, main_task.cancel)
    loop.add_signal_handler(SIGTERM, main_task.cancel)

    try:
        _LOGGER.info("Hello World !")

        # Add business code here ...

    except CancelledError:
        _LOGGER.info("Exiting...")

        # do cleanup here.

    return 0

if __name__ == "__main__":
    from sys import version_info
    assert version_info >= (3, 7), "Script requires Python 3.7+"

    from argparse import ArgumentParser

    description = "Asyncio template main"
    parser = ArgumentParser(description=description)
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug mode')

    # Add more argument here ...

    args = parser.parse_args()

    from logging import DEBUG
    from logging import INFO
    from logging import basicConfig

    # Configure logging
    if args.debug:
        logging_format = ("[%(asctime)s] %(levelname)s [%(name)s:%(lineno)d] "
                          "%(message)s")
        logging_level = DEBUG
    else:
        logging_format = "[%(asctime)s] %(levelname)s %(message)s"
        logging_level = INFO

    basicConfig(level=logging_level, format=logging_format)

    # Let's roll !
    from asyncio import run

    try:
        exit(run(main()))
    except Exception:
        _LOGGER.exception("unexpected error")

    exit(1)
